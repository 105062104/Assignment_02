var menuState = {
    preload: function() {
        game.load.bitmapFont('desyrel', 'desyrel.png', 'desyrel.xml');
        
    },

    create: function() {
        // Add a background image 
        game.add.image(0, 0, 'background');
        // Display the name of the game
        var nameLabel = game.add.bitmapText(game.width/2, 120,'desyrel', 'Mario down the stairs',60); 
        nameLabel.anchor.setTo(0.5, 0.5);
        // Show the score at the center of the screen
        var startgame = game.add.sprite(game.width/2, (game.height/3) * 1.5,'button_start'); 
        
        var startgameLabel = game.add.bitmapText(game.width/2+5, game.height/3*1.5+3,'desyrel','Start ',40);
        startgame.anchor.setTo(0.5,0.5);
        startgameLabel.anchor.setTo(0.5,0.5);

        startgame.inputEnabled = true;
        startgame.events.onInputDown.add(this.start2,this);
        
        scoreBoard = game.add.sprite(game.width/2, (game.height/3) * 2,'button_scoreboard'); 
        var scoreBoardLabel = game.add.bitmapText(game.width/2+7, game.height/3 * 2,'desyrel','Scoreboard ',40); 
        scoreBoard.anchor.setTo(0.5, 0.5);
        scoreBoardLabel.anchor.setTo(0.5, 0.5);

        //scoreBoard = game.add.key(scoreBoard)

        scoreBoard.inputEnabled = true;
        scoreBoard.events.onInputDown.add(this.scoreBoard,this);

        // Quit
        var quit = game.add.sprite(game.width/2, (game.height/3) * 2.5,'button_start'); 
        var quitLabel = game.add.bitmapText(game.width/2, game.height/3 * 2.5,'desyrel','2P',40); 
        quit.anchor.setTo(0.5, 0.5);
        quitLabel.anchor.setTo(0.5, 0.5);

        quit.inputEnabled = true;
        quit.events.onInputDown.add(this.start,this);

        //explain
        var explainLabel = game.add.text(game.width-80, game.height-15,'Click to choose',{font: '20px Arial', fill: '#ffffff' }); 
        explainLabel.anchor.setTo(0.5, 0.5);

    },
    
    scoreBoard: function(){
        game.state.start('scoreBoard');
    },
    start: function() {
        // Start the actual game 
        game.state.start('twoplayer');
    }, 
    start2: function() {
        game.state.start('input');
    },
};