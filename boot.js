var bootState = { 
    preload: function () {
    
    },
    create: function() {
        // Set some game settings. 
        game.stage.backgroundColor = '#000000';
        //var game = new Phaser.Game(650, 500, Phaser.AUTO, 'canvas');
        game.physics.startSystem(Phaser.Physics.ARCADE); 
        game.renderer.renderSession.roundPixels = true;
        // Start the load state.
        game.state.start('load');
    }
};