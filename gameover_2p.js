var gameover2pState = {

    create: function() {
        //game.stage.backgroundColor = "#000000"
        game.add.image(0, 0, 'background'); 

        //var startgameLabel = game.add.bitmapText(game.width/2+5, game.height/3*1.5+3,'desyrel','Start ',40);
        var againbtn = game.add.sprite(game.width/3, game.height/2 ,'button_start');
        againbtn.anchor.setTo(0.5,0.5);
        var againlabel = game.add.bitmapText(game.width/3, game.height/2 ,'desyrel','Again',35);
        againlabel.anchor.setTo(0.5,0.5); 

        var menubtn = game.add.sprite(game.width/3*2, game.height/2 ,'button_start');
        menubtn.anchor.setTo(0.5,0.5);
        var menulabel = game.add.bitmapText(game.width/3*2, game.height/2 ,'desyrel','Menu',35);
        menulabel.anchor.setTo(0.5,0.5); 

        againbtn.inputEnabled = true;
        againbtn.events.onInputDown.add(this.play,this);

        menubtn.inputEnabled = true;
        menubtn.events.onInputDown.add(this.start,this);
    },
    play: function() {
        game.state.start('twoplayer');
    },
    start: function() {
        game.state.start('menu');
    }
}