// Initialize Phaser
var game = new Phaser.Game(650, 500, Phaser.AUTO, 'canvas'); // Define our global variable
game.global = { score: 0 };
game.global = { myloop: true};
game.global = { weaponloop: true};
game.global = { weaponlastloop: true};
game.global = { sprite: 0};
game.global = { platform: 0};
game.global = { temp: 0};
game.global = { up: 0 };
game.global = { up2: 0 };
game.global = { uparrow: 0 };
game.global = { flipFlop: 0 };
game.global = { flipFlop2: 0 };
game.global = { scoreBoard: 0 };
game.global = { output: 0};
game.global = { username: 0};
game.global = { bmd: 0 };
game.global = { total: 0};
game.global = { bmpText: 0 };

// Add all the states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('input', inputState);
game.state.add('twoplayer', twoplayerState);
game.state.add('scoreBoard', scoreBoardState);
game.state.add('play', playState);
game.state.add('gameover', gameoverState);
game.state.add('gameover_2p', gameover2pState);

// Start the 'boot' state
game.state.start('boot');