var inputState = {
    
    create: function() {
        game.add.image(0, 0, 'background');
        var word = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var correct = [];
        //  Here we'll create a simple array where each letter of the word to enter represents one element:
        for (var i = 0; i < word.length; i++)
        {
            correct[word[i]] = false;
        }

        //  This is our BitmapData onto which we'll draw the word being entered
        bmd = game.make.bitmapData(800, 500);
        bmd.context.font = '30px Rock Salt';
        bmd.context.fillStyle = '#ffffff';
        bmd.addToWorld();

        //  Capture all key presses
        game.input.keyboard.addCallbacks(this, null, null, this.keyPress);

        //var first = game.add.bitmapText(100, 175,'desyrel','1.',55);
        var text = game.add.bitmapText(40, 160, 'desyrel','Please type your name:',40);
        var text2 = game.add.text(200, 230, '_____________________', { font: "30px Arial", fill: "#ffffff" });

        //var scoreLabel = game.add.text(game.width/2, game.height/2, 'Score: '+ score, { font: '50px Rock Salt', fill: '#ff0000' });

        var text3 = game.add.text(200, 270, '(No more than ten letters and English only)', { font: '15px', fill: '#ffffff' });
        this.x = 0;
        this.y = 0;
        output = [];
        username = 0;
    },

    keyPress: function(char) {
        var word = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        var positionX = [260,285,310,335,360,385,410,435,460,485];
        
        //  Clear the BMD
        bmd.cls();
        
        //  Loop through each letter of the word being entered and check them against the key that was pressed
        for (var i = 0; i < word.length; i++)
        {
            var letter = word.charAt(i);

            if (char === letter)
            {
                output[this.x] = letter;
                this.x += 1;
                for(var j = 0; j<this.x; j++){
                    bmd.context.fillText (output[j],positionX[j],250);
                }
            }
        }
        var backspaceKey = game.input.keyboard.addKey(Phaser.Keyboard.BACKSPACE);
        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);

        backspaceKey.onDown.add(this.back, this); 
        enterKey.onDown.add(this.start, this); 
    
    },
    back: function() {
        var positionX = [260,285,310,335,360,385,410,435,460,485];
        this.x -= 1;
        output[this.x] = 0;

        bmd.cls();
        for(var j = 0; j<this.x; j++){
            bmd.context.fillText (output[j],positionX[j],250);
        }
    },
    start: function() {
        for(var i = 0; i < output.length ; i++){
            if(username != 0){
                if(output[i] != 0){
                    username = username + output[i].toString();
                }
            }else {
                username = output[i].toString();
            }
        }
        // Start the actual game 
        game.state.start('play');
        
    }
    
}