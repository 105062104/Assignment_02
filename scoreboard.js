var scoreBoardState = { 
//var score = document.getElementById('score');
//var scoreBoard = document.getElementById('');


create: function() {
    game.add.image(0, 0, 'background');

    var database = firebase.database();
    var scoreRef = database.ref().child('score2');

    var total_post = [];
    var total_post2 = [];
    
    //board.context.fillText (this.total,50,100);
    scoreRef.orderByChild('data').limitToLast(5).once('value', function (snapshot) {
            snapshot.forEach(function(childSnapshot){
                var childData = childSnapshot.val(); 
                //console.log(childData.name);
                total_post[total_post.length] = childData.name + "<br>";
                total_post2[total_post2.length] = childData.data + "<br>" + "<br>";

                document.getElementById('scoreboard').style.fontFamily = "Rock Salt, cursive, sans-serif";
                document.getElementById("scoreboard").style.lineHeight = "2";
                document.getElementById('Score').style.fontFamily = "Rock Salt, cursive, sans-serif";
                document.getElementById("Score").style.lineHeight = "1.1";

                if(!total_post[1] && !total_post[2] && !total_post[3] && !total_post[4])
                    document.getElementById('scoreboard').innerHTML = total_post[0];
                else if(!total_post[2] && !total_post[3] && !total_post[4])
                    document.getElementById('scoreboard').innerHTML = total_post[1] + total_post[0];
                else if(!total_post[3] && !total_post[4])
                    document.getElementById('scoreboard').innerHTML = total_post[2] + total_post[1] + total_post[0];
                else if(!total_post[4])
                    document.getElementById('scoreboard').innerHTML = total_post[3] + total_post[2] + total_post[1] + total_post[0];
                else
                    document.getElementById('scoreboard').innerHTML = 
                        total_post[4] + total_post[3] + total_post[2] + total_post[1] + total_post[0];

                if(!total_post2[1] && !total_post2[2] && !total_post2[3] && !total_post2[4])
                    document.getElementById('Score').innerHTML = total_post2[0];
                else if(!total_post2[2] && !total_post2[3] && !total_post2[4])
                    document.getElementById('Score').innerHTML = total_post2[1] + total_post2[0];
                else if(!total_post2[3] && !total_post2[4])
                    document.getElementById('Score').innerHTML = total_post2[2] + total_post2[1] + total_post2[0];
                else if(!total_post2[4])
                    document.getElementById('Score').innerHTML = total_post2[3] + total_post2[2] + total_post2[1] + total_post2[0];
                else
                    document.getElementById('Score').innerHTML = 
                        total_post2[4] + total_post2[3] + total_post2[2] + total_post2[1] + total_post2[0];
            });
        })
        .catch(e => console.log(e.message));  
        /******************* */
        var gamepic = game.add.sprite(game.width/2, game.height/7 ,'label'); 
        var gameLabel = game.add.bitmapText(game.width/2, game.height/7 ,'desyrel','Scoreboard',40); 
        gamepic.anchor.setTo(0.5,0.5);
        gameLabel.anchor.setTo(0.5,0.5);
        var first = game.add.bitmapText(100, 175,'desyrel','1.',55);
        first.anchor.setTo(0.5,0.5);
        var second = game.add.bitmapText(100, 245,'desyrel','2.',55);
        second.anchor.setTo(0.5,0.5);
        var third = game.add.bitmapText(100, 315,'desyrel','3.',55);
        third.anchor.setTo(0.5,0.5);
        var fourth = game.add.bitmapText(100, 385,'desyrel','4.',55);
        fourth.anchor.setTo(0.5,0.5);
        var fifth = game.add.bitmapText(100, 455,'desyrel','5.',55);
        fifth.anchor.setTo(0.5,0.5);

        var backbtn = game.add.sprite(game.width-60, game.height-30 ,'button_back');
        backbtn.anchor.setTo(0.5,0.5); 
        var backlabel = game.add.bitmapText(game.width-60, game.height-30 ,'desyrel','Back',18);
        backlabel.anchor.setTo(0.5,0.5); 

        backbtn.inputEnabled = true;
        backbtn.events.onInputDown.add(this.start,this);
        //backbtn.events.onInputDown.add(board.cls());
},
start: function() {
    document.getElementById('scoreboard').innerHTML = "";
    document.getElementById('Score').innerHTML = "";
    game.state.start('menu');
},

    

};