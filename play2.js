var twoplayerState = {

    create: function() {
        //game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'background'); 
        //game.physics.startSystem(Phaser.Physics.ARCADE);
        //game.renderer.renderSession.roundPixels = true;

        this.cursor = game.input.keyboard.createCursorKeys();
        
        this.player = game.add.sprite(game.width/2, game.height/2, 'player');
        this.player.facingLeft = false;
        
        this.player.animations.add('rightwalk', [1, 2], 8, true);
        this.player.animations.add('leftwalk', [3, 4], 8, true);
        this.player.animations.add('rightjump', [5, 6], 8, false);
        this.player.animations.add('leftjump', [7, 8], 8, false);

         //player2
         this.player2 = game.add.sprite(game.width/2, game.height/2, 'player2');
         this.player2.facingLeft = false;
         this.player2.animations.add('left', [0, 1, 2, 3], 6);
         this.player2.animations.add('right', [9, 10, 11, 12], 6);

        // Add ceiling
        this.ceilings = game.add.group();
        this.ceilings.enableBody = true;   

        var ceiling = game.add.sprite(8, -8, 'ceiling', 0, this.ceilings); 
        ceiling = game.add.sprite(105, -8, 'ceiling', 0, this.ceilings); 
        ceiling = game.add.sprite(202, -8, 'ceiling', 0, this.ceilings);
        ceiling = game.add.sprite(299, -8, 'ceiling', 0, this.ceilings);
        ceiling = game.add.sprite(396, -8, 'ceiling', 0, this.ceilings);
        ceiling = game.add.sprite(493, -8, 'ceiling', 0, this.ceilings);
        ceiling = game.add.sprite(590, -8, 'ceiling', 0, this.ceilings);
        this.ceilings.setAll('body.immovable', true);      

        // Add platform
        this.platforms = game.add.group();
        this.platforms.enableBody = true;        
        // The platform will be in group 'this.platforms'
        platform = game.add.sprite(300, 320, 'normal', 0, this.platforms); 
        platform = game.add.sprite(400, 450, 'normal', 0, this.platforms); 
        // platform should be immovable.

        game.time.events.loop(700, this.addPlatform, this);
        this.platforms.setAll('body.immovable', true);
           
        // Add nails
        this.nails = game.add.group();
        this.nails.enableBody = true;
        game.time.events.loop(5000, this.addNails, this);
        this.nails.setAll('body.immovable', true);

        /// Add a wall on left
        this.wall = game.add.sprite(638, 0, 'wall');
        //this.blueBlock.animations.add('Bblockanim', [0, 1, 2, 3], 8,  true);
        game.physics.arcade.enable(this.wall);
        this.wall.body.immovable = true;
        
        // Add a wall on right
        this.wall2 = game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(this.wall2);
        this.wall2.body.immovable = true;

        /// Particle
        this.emitter = game.add.emitter(422, 320, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 500;

        //******** Weapon Fire **********************************/

        weapon = game.add.weapon(40, 'bullet');

    //  The 'rgblaser.png' is a Sprite Sheet with 80 frames in it (each 4x4 px in size)
    //  The 3rd argument tells the Weapon Plugin to advance to the next frame each time
    //  a bullet is fired, when it hits 80 it'll wrap to zero again.
    //  You can also set this via this.weapon.bulletFrameCycle = true
        weapon.setBulletFrames(0, 80, true);
        weapon.bulletFrameCycle = true;
        weapon.autoExpandBulletsGroup = true;
        //console.log(weapon);
    
        weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
    
        //  The speed at which the bullet is fired
        weapon.bulletSpeed = 400;
    
        //  Speed-up the rate of fire, allowing them to shoot 1 bullet every 50ms
        weapon.fireRate = 200;
    
        //  Set the weapon to autofire - you can toggle this on and off during play as well
        //weapon.autofire = true;
        sprite = this.add.sprite(50, 600, 'ship');

        var time = game.rnd.pick([13000,16000,18000,20000,22000,24000,26000]);
        weaponloop = game.time.events.loop(time, this.fireweapon, this);

        //**************************************** */

        /*************** up arrow *******************************/
        this.uparrows = game.add.group();
        this.uparrows.enableBody = true;
        game.time.events.loop(10000, this.adduparrows, this);
        this.uparrows.setAll('body.immovable', true);
        this.player1up = game.add.text(30, game.height-25, 'Player1 ', { font: '20px Arial', fill: '#ffffff' });
        this.upLabel = game.add.text(110, game.height-25, 'jumps: 0', { font: '20px Arial', fill: '#ffffff' });
        this.player2up = game.add.text(470, game.height-25, 'Player2 ', { font: '20px Arial', fill: '#ffffff' });
        this.upLabel2 = game.add.text(550, game.height-25, 'jumps: 0', { font: '20px Arial', fill: '#ffffff' });
        this.up = 0;
        this.up2 = 0;
        /*************************** *****************************/

        username = 0;
        game.physics.arcade.enable(this.player);
        game.physics.arcade.enable(this.player2);

        // Add vertical gravity to the player
        this.player.body.gravity.y = 500;
        this.player2.body.gravity.y = 500;

    },

    addPlatform: function() {
        
        var newPositionX = game.rnd.pick([1,2,3,4,5,6,7,8,9]);
        var newPositionX2 = game.rnd.pick([50,51,52,53,54,55,56,57,58,59,60]);
       
        platform = game.add.sprite(newPositionX2 * newPositionX , 400 + 100, 'normal', 0, this.platforms);
        //console.log(platform.y);
        platform.body.immovable = true;

    },

    addNails: function() {
        var newPositionY = [50,60,70,80,90,110,120,130,140,150];

        for (var i = 0; i < newPositionY.length; i++) {
            if (newPositionY[i] == platform.y) {
                newPositionY.splice(i, 1);
            }
        }

        var newPositionX = game.rnd.pick([1,2,3,4,5,6,7,8,9]);
        var newPositionX2 = game.rnd.pick([50,51,52,53,54,55,56,57,58,59,60]);

        var newPosition = game.rnd.pick(newPositionY);
        var nail = game.add.sprite(newPositionX2 * newPositionX , 500 + newPosition, 'nails', 0, this.nails);
        nail.body.immovable = true;
    },

    fireweapon: function() {
        sprite.kill();
        var Yposition = game.rnd.pick([50,80,110,140,170,200,230,260,290,320,350,380,410,440,470,500]);
        sprite = this.add.sprite(50, Yposition, 'ship');
    
        sprite.anchor.set(0.5);
    
        game.physics.arcade.enable(sprite);
    
        sprite.body.drag.set(70);
        sprite.body.maxVelocity.set(200);
    
        //  Tell the Weapon to track the 'player' Sprite
        //  With no offsets from the position
        //  But the 'true' argument tells the weapon to track sprite rotation
        weapon.trackSprite(sprite, 0, 0, true);

        weapon.autofire = true;
        weaponlastloop = game.time.events.loop(4000, this.fireweaponlast, this);
        
    },
    fireweaponlast: function(){
        game.time.events.remove(this.weaponlastloop);
        game.time.events.remove(this.weaponloop);
        sprite.kill();
        weapon.autofire = false;
    },

    adduparrows: function() {
        
        uparrow = game.add.sprite(platform.x+10, platform.y-45, 'up_arrow', 0, this.uparrows);
        uparrow.body.immovable = true;
    },

    canjump: function() {
        uparrow.kill();
        this.up += 1;
        this.upLabel.text = 'jumps: ' + this.up;
    },
    canjump2: function() {
        uparrow.kill();
        this.up2 += 1;
        this.upLabel2.text = 'jumps: ' + this.up2;
    },
    update: function() {
        game.physics.arcade.collide(this.player, this.wall);
        game.physics.arcade.collide(this.player, this.wall2);
        game.physics.arcade.collide(this.player, this.platforms);
        game.physics.arcade.collide(this.player, this.ceilings,this.playerDie,null,this);
        game.physics.arcade.collide(this.player, this.nails,this.playerDie,null,this);
        game.physics.arcade.collide(this.player, weapon.bullets,this.playerDie,null,this);
        game.physics.arcade.collide(this.player, this.uparrows,this.canjump,null,this);

        game.physics.arcade.collide(this.player2, this.wall);
        game.physics.arcade.collide(this.player2, this.wall2);
        game.physics.arcade.collide(this.player2, this.platforms);
        game.physics.arcade.collide(this.player2, this.ceilings,this.player2Die,null,this);
        game.physics.arcade.collide(this.player2, this.nails,this.player2Die,null,this);
        game.physics.arcade.collide(this.player2, weapon.bullets,this.player2Die,null,this);
        game.physics.arcade.collide(this.player2, this.uparrows,this.canjump2,null,this);

        this.uparrows.setAll('body.velocity.y',-100);
        this.platforms.setAll('body.velocity.y', -100); 
        this.nails.setAll('body.velocity.y', -100);

        if (!this.player.inWorld) {this.playerDie();}
        if (!this.player2.inWorld) {this.player2Die();}
        this.movePlayer();
        this.movePlayer2();
    }, 
    playerDie: function() {
        //game.stage.backgroundColor = "#000000";

        var deadLabel = game.add.bitmapText(game.width/2, game.height/2, 'desyrel','Player2 win!', 60);
        deadLabel.anchor.setTo(0.5, 0.5);
        console.log(deadLabel.text);
        this.player.kill();
        this.player2.kill();
        this.ceilings.kill();
        this.wall.kill();
        this.wall2.kill();
        this.platforms.kill();
        this.nails.kill();
        sprite.kill();
        weapon.autofire = false;
        weapon.bullets.kill();
        this.uparrows.kill();
        
        game.time.events.add(1000,this.finishstate , this);
        //game.state.start('menu');
    },
    player2Die: function() {
        //game.time.events.remove(myloop);
        game.stage.backgroundColor = "#000000";
        
        var deadLabel = game.add.bitmapText(game.width/2, game.height/2, 'desyrel','Player1 win',60);
        deadLabel.anchor.setTo(0.5, 0.5);

        //console.log(deadLabel.text);
        this.player.kill();
        this.player2.kill();
        this.ceilings.kill();
        this.wall.kill();
        this.wall2.kill();
        this.platforms.kill();
        this.nails.kill();
        sprite.kill();
        weapon.autofire = false;
        weapon.bullets.kill();
        this.uparrows.kill();
        
        game.time.events.add(1000,this.finishstate , this);
        //game.state.start('menu');
    },

    finishstate: function() {    
        document.getElementById('result').innerHTML = "";
        game.state.start('gameover_2p');
    },

    /// ToDo: Finish the 4 animation part.
    movePlayer: function() {
        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -200;
            this.player.facingLeft = true;

            /// 1. Play the animation 'leftwalk'
                // Left animation 
                this.player.animations.play('leftwalk');
            ///
        }
        else if (this.cursor.right.isDown) { 
            this.player.body.velocity.x = 200;
            this.player.facingLeft = false;

            /// 2. Play the animation 'rightwalk' 
                // right animation 
                this.player.animations.play('rightwalk');
            ///
        }    

        // If the up arrow key is pressed, And the player is on the ground.
        else if(this.cursor.up.isDown){
            if(flipFlop == 0){
                if(this.up > 0){
                    // Move the player upward (jump)
                    if(this.player.facingLeft) {
                        /// 3. Play the 'leftjump' animation
                        this.player.animations.play('leftjump');
                        ///
                    }else {
                        /// 4. Play the 'rightjump' animation
                        this.player.animations.play('rightjump');
                        ///
                    }
                    this.player.body.velocity.y = -350;
                    this.up--;
                    //console.log(this.up);
                    this.upLabel.text = 'jumps: ' + this.up;
                    flipFlop = 1;
                }
            }        
        }  

        // If neither the right or left arrow key is pressed
        else {
            
            if(this.cursor.up.isUp){
                flipFlop = 0;
            }
            // Stop the player 
            this.player.body.velocity.x = 0;
        
            if(this.player.facingLeft) {
                // Change player frame to 3 (Facing left)
                this.player.frame = 3;
            }else {
                // Change player frame to 1 (Facing right)
                this.player.frame = 1;
            }

            // Stop the animation
            this.player.animations.stop();
        }    
    },
    movePlayer2: function() {
        var leftKey = game.input.keyboard.addKey(Phaser.Keyboard.A); // Call callback function when key is down. 
        var rightKey = game.input.keyboard.addKey(Phaser.Keyboard.D); // Call callback function when key is down. 
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.W); // Call callback function when key is down.
        if(leftKey.isDown){
            this.player2.body.velocity.x = -200;
            this.player2.facingLeft = true;

            /// 1. Play the animation 'leftwalk'
                // Left animation 
            this.player2.animations.play('left');
        } 
        else if (rightKey.isDown) { 
            this.player2.body.velocity.x = 200;
            this.player2.facingLeft = false;

            /// 2. Play the animation 'rightwalk' 
                // right animation 
                this.player2.animations.play('right');
            ///
        }    

        // If the up arrow key is pressed, And the player is on the ground.
        else if(upKey.isDown){
            if(flipFlop2 == 0){
                if(this.up2 > 0){
                    this.player2.body.velocity.y = -350;
                    this.up2--;
                    //console.log(this.up);
                    this.upLabel2.text = 'jumps: ' + this.up2;
                    flipFlop2 = 1;
                }
            }        
        }  

        // If neither the right or left arrow key is pressed
        else {
            
            if(upKey.isUp){
                flipFlop2 = 0;
            }
            // Stop the player 
            this.player2.body.velocity.x = 0;
        
            if(this.player2.facingLeft) {
                // Change player frame to 3 (Facing left)
                this.player2.frame = 3;
            }else {
                // Change player frame to 1 (Facing right)
                this.player2.frame = 1;
            }

            // Stop the animation
            this.player2.animations.stop();
        }    

    },
    /*player2left: function() {
            this.player2.body.velocity.x = -200;
            this.player2.facingLeft = true;

            /// 1. Play the animation 'leftwalk'
                // Left animation 
                this.player2.animations.play('left');
            ///
    },
    player2right: function() {
            this.player2.body.velocity.x = 200;
            this.player2.facingLeft = false;

            /// 2. Play the animation 'rightwalk' 
                // right animation 
            this.player2.animations.play('right');
            ///
    },
    player2up: function() {
        if(flipFlop == 0){
            if(this.up > 0){
                // Move the player upward (jump)
                if(this.player.facingLeft) {
                    /// 3. Play the 'leftjump' animation
                    this.player.animations.play('leftjump');
                    ///
                }else {
                    /// 4. Play the 'rightjump' animation
                    this.player.animations.play('rightjump');
                    ///
                }
                this.player.body.velocity.y = -350;
                this.up--;
                //console.log(this.up);
                this.upLabel.text = 'jumps: ' + this.up;
                flipFlop = 1;
            }
        }        
    }*/
};

//var game = new Phaser.Game(650, 500, Phaser.AUTO, 'canvas');
//game.state.add('main', playState);
//game.state.start('main');



