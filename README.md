# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## Report
* Loading 完會顯示 menu，menu有三個選項：start、scoreboard、2p，按start 會進入單人模式，按scoreboard會顯示leaderboard，按2p會進入雙人對戰模式。
* start: 單人模式，player圖案為馬力歐。
         開始遊戲前會要求先輸入目前玩家的名字。
         遊戲主體：
         碰到天花板時gameover，碰到有尖刺的平台時也gameover，不定時會出現外星戰艦發射子彈，碰到子彈也是gameover。gameover 時會有爆炸的音效並顯示you are dead，接著跳到顯示score的畫面並可選擇再玩一次或者回到menu。再玩一次了話不需要再次輸入名字，score會存在第一次輸入的名字下。
         不定時會出現向上的箭頭，若player吃到箭頭則會獲得一次jump的機會，jump可累加，目前jump數會顯示在畫面下方。另外畫面下方也會顯示score。計分方式是每秒一分，成功活過多少秒就多少分。gameover時會將目前username和score傳入firebase realtime database。
* scoreboard：會顯示目前前五高分，若紀錄還不到五個則有多少會顯示多少。右下角有back按鈕可回到menu。
* 2p: 雙人模式，player1為馬力歐，player2為小朋友下樓梯的小朋友，與單人模式相同，碰到有尖刺的平台gameover，碰到天花板gameover，吃到向上的箭頭獲得jump的機
      會，螢幕下方會顯示player1的jump數和player2的jump數。不定時會出現外星戰艦發射子彈。不同的是沒有計分，也不會顯示在scoreboard中，遊戲結束時只會顯示目前贏家是誰，若player2先死亡會顯示player1 win，player1先死亡則會顯示player2 win，然後選擇再玩一次或回到menu。
