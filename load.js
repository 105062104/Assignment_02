var loadState = { 
    preload: function () {
        // 添加进度文字
        //var game = new Phaser.Game(650, 500, Phaser.AUTO, 'canvas');
        var progressText = game.add.text(game.world.centerX, game.world.centerY, '0%', {fontSize: '40px',fill: '#ffffff'});
        progressText.anchor.setTo(0.5, 0.5);
        // 监听加载完一个文件的事件
        game.load.onFileComplete.add(function(progress) {
            progressText.text = progress + '%';
        });
        game.add.tween(progressText).to( { alpha: 0 }, 2000, "Linear", true);
        
        // Load all game assets
        // Loat game sprites.
        game.load.image('background', 'background.jpg');
        game.load.image('wall', 'wall.png'); // 牆壁
        game.load.image('ceiling', 'ceiling.png'); // 天花板的刺 
        game.load.image('normal', 'normal.png'); // 藍色平台
        game.load.image('nails', 'platform_nails.png'); // 帶刺平台
        game.load.image('button_start', 'button_start.png');
        game.load.image('button_scoreboard', 'button_scoreboard.png');
        game.load.image('button_back','button_back.png');
        game.load.spritesheet('player', 'player.png', 32, 54);
        game.load.image('bullet', 'bullet.png');
        game.load.image('ship', 'ship.png');
        game.load.image('up_arrow','arrowup.png');
        game.load.image('label','label.png');
        game.load.spritesheet('player2', 'player2.png', 32, 32);
        game.load.audio('explosion', 'explosion.mp3');

        

        //game.load.bitmapFont('desyrel', 'assets/fonts/bitmapFonts/desyrel.png', 'assets/fonts/bitmapFonts/desyrel.xml');
    },
    
    create: function() {
        // Go to the menu state 
        //var game = new Phaser.Game(650, 500, Phaser.AUTO, 'canvas');
        setTimeout(function() {
            game.state.start('menu');
        }, 2000);
        

    } 
};
